"""
WSGI config for kenbak1 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

"""import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "kenbak1.settings")

application = get_wsgi_application()"""

#
#
#
#
#

"""
WSGI config for kenbak1 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

# Also import sys and site to help enable virtualenv
import os, sys, site


# As is
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "kenbak1.settings")


try:
    # Add the site packages, to override any system-wide packages
    site.addsitedir('/home/blankenbaker/webapps/kenbak1/kenbak1/venv/lib/python2.7/site-packages')
    
    # Activate the virtualenv
    activate_this = os.path.expanduser(os.path.abspath('/home/blankenbaker/webapps/kenbak1/kenbak1/venv/bin/activate_this.py'))
    execfile(activate_this, dict(__file__=activate_this))
except Exception as e:
    print e

# Calculate the path based on the location of the WSGI script
# project = '~/webapps/kenbak1/kenbak1/'
# workspace = os.path.dirname(project)
# sys.path.append(workspace)

# sys.path = ['~/webapps/kenbak1/kenbak1', '~/webapps/kenbak1/kenbak1/kenbak1', '~/webapps/kenbak1'] + sys.path


# As is
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

