from django import forms
from django.utils.translation import ugettext as _
from django.template import loader
from django.core.mail import EmailMessage

from models import Contact

from kenbak1.settings import DEFAULT_FROM_EMAIL, DEFAULT_RECIPIENT_MAILS


class ContactForm(forms.ModelForm):
    """docstring for ContactForm"""
    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['style'] = 'text-transform: capitalize;'
        self.fields['name'].widget.attrs['placeholder'] = 'Your Name'
        self.fields['email'].widget.attrs['placeholder'] = 'Email Address'
        self.fields['subject'].widget.attrs['placeholder'] = 'Subject'
        self.fields['message'].widget.attrs['placeholder'] = 'Your Message'
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

    def send_email(self):
        context = {
            'contact_name': self.instance.name,
            'contact_email': self.instance.email,
            'contact_subject': self.instance.subject,
            'contact_message': self.instance.message,
            'protocol': 'http',
            }
        try:
            email_template_name = 'contact_email_template.html'
            # Email subject *must not* contain newlines
            subject = _("Contact Message Received")
            email = loader.get_template(email_template_name).render(context)
            recipient_list = [ DEFAULT_FROM_EMAIL ] + DEFAULT_RECIPIENT_MAILS
            # send_mail(subject, email, DEFAULT_FROM_EMAIL , recipient_list, fail_silently=False)
            email_message = EmailMessage(subject, email, to=recipient_list, from_email=DEFAULT_FROM_EMAIL)
            email_message.content_subtype = 'html'
            email_message.send()
        except Exception as e:
            print 'Exception', e
            return False
        return True

    class Meta:
        model = Contact
        fields = (
            'name',
            'email',
            'subject',
            'message',
            )
        widgets = {
            'message': forms.Textarea(attrs={'rows': 3}),
            }
