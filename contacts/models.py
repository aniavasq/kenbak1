from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext as _


class Contact(models.Model):
    """docstring for Contact"""
    name = models.CharField(max_length=128, verbose_name=_('Name'))
    email = models.EmailField(max_length=254, verbose_name=_('Email'))
    subject = models.CharField(max_length=128, verbose_name=_('Subject'))
    message = models.CharField(max_length=254, verbose_name=_('Email'))
    deleted = models.BooleanField(default=False)

    def delete(self):
        self.deleted = True
        self.save()
