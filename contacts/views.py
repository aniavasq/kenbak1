from django.views.generic.edit import CreateView
from django.utils.translation import ugettext as _
from django.urls import reverse_lazy
from django.shortcuts import redirect

from models import Contact
from forms import ContactForm

class ContactView(CreateView):
    model = Contact
    form_class = ContactForm
    permission_required = ('exameneslab.add_examenlaboratorio',)
    template_name = 'contact.html'
    s_param = None

    def get_success_url(self):
        if self.s_param:
            return reverse_lazy('contact', kwargs={'at': self.s_param})
        else:
            return reverse_lazy('contact')

    def form_invalid(self, form):
        self.s_param = 'paswfm'
        # return super(ContactView, self).form_invalid(form)
        return redirect('contact', at=(self.s_param))

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        if form.send_email():
            self.s_param = 'paswsm'
            return super(ContactView, self).form_valid(form)
        else:
            return super(ContactView, self).form_invalid(form)

    def get_alert_message(self):
        alert_message = None
        alert_type = self.kwargs.get('at', None)
        if alert_type:
            if alert_type == 'paswsm':
                alert_message = {
                    'class': 'success',
                    'mod': _('Thanks!'),
                    'message': _("We are mailing you after read the message.")
                    }
            elif alert_type == 'paswfm':
                alert_message = {
                    'class': 'danger',
                    'mod': _('Sorry'),
                    'message': _("The message couldn't be sent.")
                    }
        return alert_message


    def get_context_data(self, **kwargs):
        context = super(ContactView, self).get_context_data(**kwargs)
        context['alert_message'] = self.get_alert_message()
        return context
